app.controller('GenericCtrl', function($scope,$http) {
	$scope.numberArr = [];
    $scope.dealterm;
    $scope.interpretation = {};

    $scope.dealtype = ["Benchmark","Past","Competitor"];
    $scope.clientIndustryDto = ["Financials","Healthcare","Infrastructure","Metals","Retail and services"];
    $scope.dealPhase = ["Rfi","Rfp","Bafo"];
    $scope.country = ["Europe", "Great Britain", "Sweden", "Norway"];
    $scope.currency = ["Euro", "GBP", "SEK","NOK"]

    $scope.offshoreAndHardwareIncluded = ["Yes","No"];
    $scope.standardwindowInfo = ["High", "Medium", "Low"];
    $scope.genericDealInfoDto = [];
    
    $scope.getNumber = function(dealterm) {
    	$scope.numberArr = [];
	    for(var i=0;i<$scope.dealterm;i++)
	    {
	    	$scope.numberArr.push(i)  
	    }
    }

    $scope.$watch('dealterm', function(value) {
    	$scope.getNumber($scope.dealterm);
    });
  
    $scope.init=function(){
	        $http({
	            method : 'GET',
	            url : 'http://localhost:8094/DealPricingService/genericDealInfo/getDealInfo'
	        }).then(function successCallback(response) {
	            $scope.genericDealInfoDto = response.data;
	        }, function errorCallback(response) {
	            console.log(response.statusText);
	        });
    }
	$scope.init();

  
});

