app.controller('HomeCtrl', function($rootScope,$location) {
	$rootScope.activeFor = function (page) {
	    var currentRoute = $location.path().substring(1);
	    return page === currentRoute ? 'active' : '';
	};
	$rootScope.loadGeneric = function () {
        $location.url('/generic');
    };
    
    $rootScope.loadHosting = function () {
    	$location.url('/hosting');
    };
	  
});
