var app = angular.module('app', [ 'ngRoute', 'ngResource' ]);
app.config(function($routeProvider, $locationProvider) {
	$routeProvider
	.when('/login', {
		templateUrl : '/views/login.html',
		controller : 'LoginCtrl'
	}).when('/home', {
		templateUrl : '/views/home.html',
		controller : 'HomeCtrl'
	}).when('/generic', {
		templateUrl : '/views/generic.html',
		controller : 'GenericCtrl'
	}).when('/hosting', {
		templateUrl : '/views/hosting.html',
		controller : 'hostingController'
	}).otherwise({
		redirectTo : '/login'
	});

	$locationProvider.html5Mode({
	  enabled: true,
	  requireBase: false
	});
});
